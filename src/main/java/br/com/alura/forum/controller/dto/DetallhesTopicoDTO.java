package br.com.alura.forum.controller.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import br.com.alura.forum.modelo.StatusTopico;
import br.com.alura.forum.modelo.Topico;

public class DetallhesTopicoDTO {

	private long id;

	private String titulo;

	private String mensagem;

	private LocalDateTime dataCriacao;

	private String nomeAutor;

	private StatusTopico status;

	private List<RespostaDTO> respostas;

	public DetallhesTopicoDTO(Topico topico) {
		this.id = topico.getId() != null ? topico.getId() : 0;
		this.titulo = topico.getTitulo();
		this.dataCriacao = topico.getDataCriacao();
		this.mensagem = topico.getMensagem();
		this.nomeAutor = topico.getAutor() != null ? topico.getAutor().getNome() : null;
		this.status = topico.getStatus();
		this.respostas = topico.getRespostas().stream().map(RespostaDTO::new).collect(Collectors.toList());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getNomeAutor() {
		return nomeAutor;
	}

	public void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}

	public StatusTopico getStatus() {
		return status;
	}

	public void setStatus(StatusTopico status) {
		this.status = status;
	}

	public List<RespostaDTO> getRespostas() {
		return respostas;
	}

	public void setRespostas(List<RespostaDTO> respostas) {
		this.respostas = respostas;
	}

}
