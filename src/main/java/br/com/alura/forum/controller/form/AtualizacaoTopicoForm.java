package br.com.alura.forum.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.alura.forum.modelo.Topico;
import br.com.alura.forum.repository.TopicoRepository;

public class AtualizacaoTopicoForm {

	@NotNull(message = "Insira o titulo")
	@NotEmpty(message = "Insira o titulo")
	@Length(max = 50, message = "Se liga no tamanho desse trem")
	private String titulo;

	@NotNull(message = "Insira a mensagem")
	@NotEmpty(message = "Insira a mensagem")
	@Length(max = 1000, message = "Se liga no tamanho desse mensagem")
	private String mensagem;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Topico atualizar(Long id, TopicoRepository topicoRepository) {
		Topico topico = topicoRepository.getOne(id);

		topico.setTitulo(this.titulo);
		topico.setMensagem(this.mensagem);

		topicoRepository.save(topico);

		return topico;
	}
}
