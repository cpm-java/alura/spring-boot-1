package br.com.alura.forum.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.alura.forum.modelo.Topico;
import br.com.alura.forum.repository.CursoRepository;

public class TopicoForm {

	@NotNull(message = "Insira o titulo")
	@NotEmpty(message = "Insira o titulo")
	@Length(max = 50, message = "Se liga no tamanho desse trem")
	private String titulo;

	@NotNull(message = "Insira a mensagem")
	@NotEmpty(message = "Insira a mensagem")
	@Length(max = 1000, message = "Se liga no tamanho desse mensagem")
	private String mensagem;

	@NotNull(message = "Insira o curso")
	@NotEmpty(message = "Insira o curso")
	@Length(max = 50, message = "Se liga no tamanho desse trem")
	private String nomeCurso;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getNomeCurso() {
		return nomeCurso;
	}

	public void setNomeCurso(String nomeCurso) {
		this.nomeCurso = nomeCurso;
	}

	public Topico converter(CursoRepository cursoRepository) {
		return new Topico(this.titulo, this.mensagem, cursoRepository.findByNome(nomeCurso));
	}
}